package main
import "fmt"

func main() {
	var conferenceName = "Go Conference"
	const conferenceTickets = 50

	var remainingTickets = 50

	fmt.Printf("Welcome to our %v booking application.\n", conferenceName)
	fmt.Printf("We have a total of %v tickets and %v tickets are available for booking.\n", conferenceTickets, remainingTickets)
	fmt.Printf("Get your tickets here to attend.\n")

	var userName string
	var userTickets int

	userName = "sumit kushwah"
	userTickets = 2
	fmt.Printf("User %v booked %v tickets.\n", userName, userTickets)

}
